package alltasks.task2;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View {
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private Scanner input;
    //private static Logger logger = LogManager.getLogger(Controller.class);

    public View() {
        controller = new Controller();
        input = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        menu.put("3", "\n  3 - Put word and get it with UPPER letters");
        menu.put("4", "\n  4 - Put word and get it with low letters");
        menu.put("5", "\n  5 - Put word and get is Empty or not");
        menu.put("6", "\n  6 - Show three randoms Lists");
        menu.put("Q", "\n  Q - exit\n");
        methodsMenu = new LinkedHashMap<>();

        methodsMenu.put("3", this::printWordWithUpperLetter);
        methodsMenu.put("4", this::printWordWithLowLetter);
        methodsMenu.put("5", this::printIsEmptyOrNot);
        methodsMenu.put("6", this::printThreeRandomsLists);

    }



    private String putWord() {
        System.out.println("Put your word:");
        return input.next();
    }

    private void printWordWithUpperLetter() {
        System.out.println(controller.commandToUpperCase.command(putWord()));
    }

    private void printWordWithLowLetter() {
        System.out.println(controller.commandToLowerCase.command(putWord()));
    }

    private void printIsEmptyOrNot() {
        System.out.println(controller.isEmpty(putWord()));
    }

    private void printThreeRandomsLists() {
        System.out.println("First list:");
        System.out.println(controller.firstRandomList.random());
        System.out.println("Second list:");
        System.out.println(controller.secondRandomList.random());
        System.out.println("Third list:");
        System.out.println(controller.thirdRandomList.random());
    }
    public void show() {
        String keyMenu;
        do {

            System.out.println("\nPlease, select menu point:\n");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                e.getStackTrace();
            }
        } while (!keyMenu.equals("Q"));
    }
}
