package alltasks.task2;
@FunctionalInterface
public interface Command {
    String command(String arg);
}
