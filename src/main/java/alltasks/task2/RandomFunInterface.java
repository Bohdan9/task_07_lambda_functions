package alltasks.task2;

import java.util.List;

@FunctionalInterface
public interface RandomFunInterface {
    List<Integer> random();
}