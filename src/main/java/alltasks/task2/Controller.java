package alltasks.task2;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

 class Controller {
    Command commandToUpperCase = String::toUpperCase;

    Command commandToLowerCase = String::toLowerCase;
    String isEmpty(String argument) {
        return new CommandObj().command(argument);
    }

    RandomFunInterface firstRandomList = () -> {
        List<Integer> list = new ArrayList<>();
        randomGenerator(list);
        return list;
    };

    RandomFunInterface secondRandomList = () -> {
        List<Integer> list = new ArrayList<>();
        randomGenerator(list);
        return list;
    };

    RandomFunInterface thirdRandomList = () -> {
        List<Integer> list = new ArrayList<>();
        randomGenerator(list);
        System.out.println(list);
        return list;
    };

    private void randomGenerator(List<Integer> list) {
        Random random = new Random();
        for (int i = 0; i < 7; i++) {
            list.add(random.nextInt(50));
        }
    }
}
