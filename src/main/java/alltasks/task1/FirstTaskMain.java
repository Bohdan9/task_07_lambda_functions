package alltasks.task1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class FirstTaskMain {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        Lambda maxValue = (a, b, c) -> (a > b) ? (a > c) ? a : c : (b > c) ? b : c;
        Lambda averageValue = (a, b, c) -> (a + b + c) / 3;
        try {
            System.out.println("Enter values to find max");
            System.out.println(maxValue.acceptThreeValues(scanner.nextInt(), scanner.nextInt(), scanner.nextInt()));
            System.out.println("Enter values to find average");
            System.out.println(averageValue.acceptThreeValues(scanner.nextInt(), scanner.nextInt(), scanner.nextInt()));
        } catch (InputMismatchException e) {
            System.out.println("You have not entered a number.");
        }
    }
}


