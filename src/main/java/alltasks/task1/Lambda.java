package alltasks.task1;

@FunctionalInterface
public interface Lambda  {
    int acceptThreeValues(int first, int second, int third);
}
