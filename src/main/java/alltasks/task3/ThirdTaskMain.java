package alltasks.task3;

import sun.util.resources.LocaleData;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class ThirdTaskMain {
    public static void main(String[] args) {

        Service thirdTaskService = new Service();
        List<Integer> randomList = thirdTaskService.getRandomIntegers();
        System.out.print("Average: " + thirdTaskService.getAverage(randomList));
        System.out.println("Min value: " + thirdTaskService.getMinValue(randomList));
        System.out.println("Max value: " + thirdTaskService.getMaxValue(randomList));
        System.out.println("Sum: " + thirdTaskService.getSumOfList(randomList));
        int biggerThanAverage = thirdTaskService.biggerThanAverageValues(randomList);
        System.out.println("Number of values bigger than average: " + biggerThanAverage);





    }
}
