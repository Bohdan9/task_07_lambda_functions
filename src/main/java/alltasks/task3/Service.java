package alltasks.task3;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Random;

class Service {

    List<Integer> getRandomIntegers() {
        List<Integer> integers = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            int randomNumber = random.nextInt(100);
            integers.add(randomNumber);
        }
        for (Integer integer : integers) {
            System.out.println(integer);

        }
        return integers;

    }

    double getAverage(List<Integer> list) {
        return list
                .stream()
                .mapToInt(values -> values)
                .average()
                .orElseThrow(NoSuchElementException::new);
    }

    int getMinValue(List<Integer> list) {
        return list
                .stream()
                .mapToInt(values -> values)
                .min()
                .orElseThrow(NoSuchElementException::new);
    }

    int getMaxValue(List<Integer> list) {
        return list
                .stream()
                .mapToInt(values -> values)
                .max()
                .orElseThrow(NoSuchElementException::new);
    }


    int getSumOfList(List<Integer> list) {
        return list
                .stream()
                .mapToInt(values -> values)
                .sum();
    }

    int biggerThanAverageValues(List<Integer> list) {
        return (int) list
                .stream()
                .filter(v -> v > getAverage(list))
                .count();
    }

}
