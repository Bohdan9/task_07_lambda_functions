package alltasks.task4;

import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;


public class Application {


        private List<String> list = new ArrayList<>();

        private void read() throws IOException {
            String line;
            try (Scanner input = new Scanner(System.in)) {

                while (!(line = input.nextLine()).isEmpty()) {
                    list.add(line);
                }
            }
        }

        private List<String> getWordsList() {
            return list.stream().flatMap(e -> Stream.of(e.split(" "))).collect(Collectors.toList());
        }

        private long countWords() {
            return getWordsList().stream().distinct().count();
        }

        private List<String> sortUniqueWords() {
            return getWordsList().stream().distinct().sorted().collect(Collectors.toList());
        }

        private Map<String, Long>uniqueWords() {
            return getWordsList().stream().collect(groupingBy(Function.identity(), counting()));
        }

        private Map<Character, Long> countCharNumber() {
            return getWordsList().toString().chars()
                    .mapToObj(c -> (char) c)
                    .filter(Character::isLowerCase)
                    .collect(groupingBy(Function.identity(), counting()));
        }

        public static void main(String[] args) {
            System.out.println("Enter your text:");
            Application app = new Application();
            try {
                app.read();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println(app.countWords());
            app.sortUniqueWords().forEach(System.out::println);
            app.uniqueWords().forEach((key, value) -> System.out.println(String.format("%s - %d", key, value)));
            app.countCharNumber().forEach((key, value) -> System.out.println(String.format("%c->%d", key, value)));
        }
}
